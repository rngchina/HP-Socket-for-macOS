#pragma once
#include <functional>
#include <string>
const std::string HTTP_METHOD_UNKNOW_TXT = "UNKNOW";
const std::string HTTP_METHOD_POST_TXT = "POST";
const std::string HTTP_METHOD_PUT_TXT = "PUT";
const std::string HTTP_METHOD_PATCH_TXT = "PATCH";
const std::string HTTP_METHOD_GET_TXT = "GET";
const std::string HTTP_METHOD_DELETE_TXT = "DELETE";
const std::string HTTP_METHOD_HEAD_TXT = "HEAD";
const std::string HTTP_METHOD_TRACE_TXT = "TRACE";
const std::string HTTP_METHOD_OPTIONS_TXT = "OPTIONS";
const std::string HTTP_METHOD_CONNECT_TXT = "CONNECT";

enum class HttpMethod : int {
	HTTP_METHOD_UNKNOW,
	HTTP_METHOD_GET,
	HTTP_METHOD_POST,
	HTTP_METHOD_PUT,
	HTTP_METHOD_DELETE,
	HTTP_METHOD_PATCH,
	HTTP_METHOD_HEAD,
	HTTP_METHOD_TRACE,
	HTTP_METHOD_OPTIONS,
	HTTP_METHOD_CONNECT,
};

std::string toUpper(const std::string &val);
std::string toString(HttpMethod m);
HttpMethod toMethod(const std::string& m);

class HttpRequest;
class HttpResponse;
using GlobalExceptionHandle = std::function<bool (const std::string&, HttpRequest*, HttpResponse*)>;