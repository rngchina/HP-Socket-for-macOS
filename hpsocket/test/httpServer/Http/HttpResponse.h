#pragma once
#include <HPSocket/HPSocket.h>
#include <string>
#include <map>
#include <sstream>
#include <mutex>

class HttpContext;
class HttpResponse final
{
    friend class HttpContext;
public:
    HttpResponse(HttpContext& context);
    ~HttpResponse();
    void setHeader(const std::string& key, const std::string& value);
    void writeData(const char* data, int len);
    void setStatusCode(EnHttpStatusCode code);
private:
    std::map<std::string, std::string> m_mapHeader;
    std::stringstream m_buffer;
    std::mutex m_mxtBuf;
    EnHttpStatusCode m_statusCode;
    HttpContext& m_cxt;
};

