#pragma once
#include <HPSocket/HPSocket.h>
#include <memory>
#include <unordered_map>
#include "HttpRouter.h"
#include "HttpHelper.h"
#include "HttpResponse.h"


class HttpRequest;
class HttpResponse;
class HttpContext
{
public:
	HttpContext(std::unordered_map<size_t,GlobalExceptionHandle>& exceptHandles);
	~HttpContext();

	void clear();
    bool handle(); //进行处理
    bool exceptionHandle(std::exception_ptr eptr);
	std::unique_ptr<HttpRequest> m_request;
	std::unique_ptr<HttpResponse> m_response;

    CONNID m_connId;
    std::shared_ptr<IHttpServer> httpServer;

    Router::callback_type callback;
    std::unordered_map<size_t,GlobalExceptionHandle>& exceptHandles;

    void wRequestBody(const char *buf, int len);
    void writeNotFount();
private:
    void respHandle();
};
