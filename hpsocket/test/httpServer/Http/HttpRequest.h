#pragma once
#include <HPSocket/HPSocket.h>
#include <string>
#include <sstream>
#include <mutex>
#include "HttpRouter.h"
#include "HttpHelper.h"

class HttpContext;
class HttpRequest final
{
    friend class HttpContext;
public:
    HttpRequest(HttpContext& context);
    std::string getBody();
    CONNID Connid() const;

private:
    std::stringstream m_buffer;
    std::mutex m_mxtBuf;
private:
    void writeData(const char* buf, int len);
    HttpContext& m_cxt;
};

